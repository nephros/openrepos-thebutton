# The Button! for SailfishOS

Proof-of-Concept of an multi-app-launcher generator for SailfishOS.

Right now this is only a shell script which will:

 1. iterate through the installed .desktop files
 2. ask the user to pick the ones to add to **The Button!**
 3. shove all the launch commands selected into a shell script and place that into `~/.local`
 4. generate a `.desktop` file which points to said shell script
 5. asks the user to name their **Button!** 
 6. randomly selects one out of four icons featuring wonderfully coloured **Button!**s for almost endless fun with different **Button!**.
