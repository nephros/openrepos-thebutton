#!/usr/bin/env bash

XDG_DESKTOP_DIR_USER=${XDG_DESKTOP_DIR_USER:=/home/${USER}/.local/share/applications}
XDG_DESKTOP_DIR_SYSTEM=${XDG_DESKTOP_DIR_SYSTEM:=/usr/share/applications}
XDDU=${XDG_DESKTOP_DIR_USER}
XDDS=${XDG_DESKTOP_DIR_SYSTEM}

DTEMPLATE=./thebutton.desktop.template
NUMICONS=4
# initialize array to collect commands in
DS=( )

###
# user may give a wildcard to filter apps
###

if [ ! -z "${PAT}" ]; then
  	PAT="*${PAT}*"
else
	PAT="*"
fi

###
# user selecting things part
###
for d in ${XDDU}/${PAT}.desktop ${XDDS}/${PAT}.desktop; do
  echo got $d
  if [ $(grep -c '^Exec' "${d}" 2>/dev/null) -gt 0 ] ; then
    echo -en "found ${d##*/}, would you like to add it to THE BUTTON?\n\ty to accept\n\tn or enter to skip\n\tq to finish selection\n\tQ to exit\n?> "
    read a
    case $a in
      y|Y)
      # collect the Exec command in our bash array
      DS+=( "${d}" )
      ;;
    n|N)
      echo "OK, skipping"
      continue
      ;;
    q)
      echo "OK, finihed"
      break
      ;;
    Q)
      exit
      ;;
    *)
      echo "answer not understood, skipping."
      ;;
    esac
  else
    continue
  fi
done

outdir=$(mktemp -d)
mkdir -p $outdir
uuid=$(cat /proc/sys/kernel/random/uuid)
outfilename=the_button_${uuid}.sh
outfile=${outdir}/${outfilename}
dtfilename=the_button_${uuid}.desktop
scriptfilepath=/home/${USER}/.local/lib/thebutton/scripts
# select a random icon:
iconfilename=the_button0$(( $RANDOM % $NUMICONS + 1)).png

###
# generate a Launcher script
### 
echo '#!/usr/bin/env bash' > ${outfile}
for e in ${DS[@]}; do
  echo looking at $e...
  grep '^Exec' "${e}" | sed 's/^Exec=//;s/\%[A-Za-z]/\$1/g;s/$/ \&/' >> ${outfile}
  # fill in a sleep each line, otherwise you get really ugly flickering as too much happens at once
  echo 'sleep 0.2' >> ${outfile}
done

###
# generate a Desktop file from the template
### 
if [[ $(wc -l ${outfile} | cut -d " " -f 1) -gt 1 ]]; then
	echo "" >> ${outfile}
	echo -n "Please give THE BUTTON a Name: "
	read name
	echo creating .desktop file...
	cp $DTEMPLATE ${outdir}/${dtfilename}
	cp icons/${iconfilename} ${outdir}/
	sed -i "s#@@name@@#${name}#;s#@@script@@#${scriptfilepath}/${outfilename}#;s#@@icon@@#${iconfilename%%.*}#" ${outdir}/${dtfilename}
	echo created things at: ${outdir}
else 
  rm -f $outdir
fi
